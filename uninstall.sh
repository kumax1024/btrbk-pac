#!/bin/sh

PATH=/usr/bin:/bin

[ -f /usr/local/bin/btrbk_pac_log ] && rm -v /usr/local/bin/btrbk_pac_log
[ -f /usr/share/libalpm/scripts/btrbk_pac_log_script ] && rm -v /usr/share/libalpm/scripts/btrbk_pac_log_script
[ -f /usr/share/libalpm/hooks/00-btrbk-pre.hook ] && rm -v /usr/share/libalpm/hooks/00-btrbk-pre.hook
[ -f /usr/share/libalpm/hooks/zx-btrbk-post.hook ] && rm -v /usr/share/libalpm/hooks/zx-btrbk-post.hook

exit 0

