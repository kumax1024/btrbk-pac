# Table of Contents

[[_TOC_]]

# Synopsis

This is a set of scripts and `pacman` hooks which automate `btrbk` processes and logs the relevant snapshot and information.

# Introduction

It is convenient to have `btrbk` snapshots created before and after pacman transactions in case a bad update effectively bricks a system.
But just having the `btrfs` snapshots thanks to `btrbk` doesn't tell you which snapshot was the cause of the problems.
Therefore, having the snapshots' names and the relevant `pacman` information logged and easily available can make it much easier to know which snapshot to return to.

The logging program can even be used standalone with custom messages, in case you want to use it for something other than backing up the root directories through `pacman` hooks.

The base of `btrbk-pac` is the `btrbk_pac_log` program, which logs a snapshot's name along with either a custom message or information about the previously run `pacman` command.
The `btrbk_pac_log` program parses information denoted by in-line comments in the `btrbk` config file, adds an entry to the desired log about the latest relevant snapshot, and then cleans the log of any entries for snapshots that have since been deleted.

There is another provided script, `btrbk_pac_log_script`, which parses information from a user's `/etc/btrbk_logger/btrbk_logger.conf` file, and then calls the `btrbk_pac_log` program with that information.

Finally, there are two `pacman` hooks which call the `btrbk_pac_log_script`, one for before the `pacman` transaction and one for after.
The hooks call the `btrbk_pac_log_script` every time `pacman` is called, but whether `btrbk` is called or any logs are updated depend on the user's configuration files.

# Installation

`Btrbk-pac` is comprised of POSIX-compliant shell scripts and plain text documents, so no additional dependencies are required outside of `btrbk`.
To install `btrbk`, follow [this link](https://github.com/digint/btrbk/blob/master/doc/install.md).

1) Clone this git repository: `git clone https://www.gitlab.com/KodyVB/btrbk-pac`
2) Change directory to the btrbk-pac directory and run `./install.sh` with root privileges\*

\* Always read through and make sure you understand any scripts that you run from the internet, especially when they're run with root privileges.

# Configuration

## Modifying btrbk configurations

To let the logging programs know which snapshots to log, the btrbk config file (defaults are `/etc/btrbk/btrbk.conf` and `/etc/btrbk.conf`) needs in-line comments on the following lines if relevant to the logging process:

* volume
* subvolume
* snapshot_dir or target
* ssh_identity

The in-line comments are denoted by "#", and contain the profile names that match the profile names in **btrbk logger configurations** subsection.
There can be multiple profiles for a single line, and they are space-delimited.
An example of a btrbk configuration file with profiles can be found in [this link](https://gitlab.com/KodyVB/btrbk-pac/-/blob/master/etc/btrbk.conf.example).
If any of the first three sections in the list above are omitted, the `btrbk_pac_log` program will print an error statement and exit out, and `btrbk_pac_log_script` will move on to the next line in the logger config file, if there is one.
If the ssh_identity comment is omitted, and that file is needed for the SSH connection to be completed, the programs will behave similarly.

## Btrbk logger configurations

The btrbk logger script, which is called by the pacman hooks, parses information from the `/etc/btrbk_logger/btrbk_logger.conf` file line by line, from top to bottom.
It only reads lines which have the following format: `<btrbk/logger run status>{btrbk config location}[profiles](log destination)`

## <btrbk/logger run status>{btrbk config location}\[profiles\] (log destination)

### <btrbk/logger run status>

This configuration allows the user to choose whether btrbk runs as well as whether the log gets updated.
It has the following options (if left blank, it defaults to "none"):

| Option | Btrbk run status before transaction | Btrbk run status after transaction | Logger run status before transaction | Logger run status after transaction |
| :---: | :---: | :---: | :---: | :---: |
| pre | Y |  | Y\* |  |
| post |  | Y |  | Y\* |
| both | Y | Y | Y\* | Y\* |
| none |  |  | Y\* | Y\* |
| pre-log |  |  | Y\* |  |
| post-log |  |  |  | Y\* |

\* No log will be updated if "none" is in the **(log destination)** section.

### {btrbk config location}

This is the absolute path to the btrbk config file.
If left blank, it searches for `/etc/btrbk/btrbk.conf` then `/etc/btrbk.conf`.
If neither of those are found after the search, the program moves on to the next line in the btrbk logger config.

### \[profiles\]

These are the comma-separated profiles which match the in-line comment profiles.
They tell the scripts where to look for the snapshots that they're logging as well as the snapshot's name, as btrbk names the snapshots based off the subvolume.
It also provides the snapshot_dir/target directory to leave the log and the ssh_identity, if relevant.
If neither a path for a log file is given nor "none" is chosen in the **(log destination)** section, it uses the snapshot_dir/target directory parsed from the profile for the log's destination.

### (log destination)

The absolute path to the desired log file.
If left blank, will default to `btrbk_snaps.log` inside of the snapshot_dir/target directory corresponding to the profile in the `btrbk` config file.
If "(none)" is used, no log file will be updated.

# SSH

You can use SSH to: store local `btrbk` setups' logs over SSH, backup via `btrbk` over SSH and store the log over SSH, or backup via `btrbk` over SSH and store the logs locally.
If the `ssh_identity` line is required to access the other computer via SSH, an in-line comment with the profile name is required, even if it's just to store a local `btrbk` setup's logs via SSH.
If the port isn't required (the default port is in use), the following format should work: `ssh://XXX.XXX.X.XX/path/to/file.log`.
Otherwise, to add the port information, the following should work: `ssh://XXX.XXX.X.XX:XXXX/path/to/file.log`.

# Standalone logger

The logging program, `btrbk_pac_log` can be called by the user without using `pacman`.
This can be useful for logging the user's home directory or other directories' snapshots.
It doesn't run `btrbk` itself, but the user could create an alias in bash/zsh/fish/etc. for ease of use if desired, which is shown below.

The mandatory arguments are:
* -c \[CONFIG FILE\]: The absolute path to the `btrbk` configuration file.
* -p \[PROFILE\]: The profile to be used from the `btrbk` configuration file.

Optional arguments are:
* -o \[LOG NAME\]: The location of the log file. Defaults to the "\[VOLUME\]/\[SNAPSHOT_DIR\]/btrbk_snaps.log" or "\[TARGET\]/btrbk_snaps.log" parsed from the `btrbk` config file.
* -t \[TYPE\]: The type of snapshot (usually pre/post). This precedes "pacman" in the log.
* -m \[MESSAGE\]: A custom message to put in { } after the snapshot name in the log.

If there were a `btrbk` config file named `/etc/btrbk/home.conf` with the "Home" profile set up, and the user wants to have the logger output to `/var/log/btrbk_home.log` with the message indicating it's a manual backup, one could use the bash/zsh alias: 

```sh
alias home_backup='sudo btrbk -c /etc/btrbk/home.conf run && btrbk_pac_log -c /etc/btrbk/home.conf -p Home -m "Manual backup" -o /var/log/btrbk_home.log'
```

